# syntax=docker/dockerfile:1
# FROM rocker/rstudio:4.1.0
FROM rocker/geospatial:4.1.0
# COPY . /home/rstudio/
# RUN git clone https://git.math.uzh.ch/fblasi/climsimstudy.git
#RUN mkdir climsimstudy

USER root
#RUN cd /home/rstudio/ && \
#	git clone https://git.math.uzh.ch/fblasi/dockertest.git && \
#	ls && \
#	cd dockertest && \
#	mkdir lib.loc estimates fields finalplots && \
#	chmod a+rwx /home/rstudio/dockertest/dockertest.Rproj && \
#	ls
WORKDIR /home/rstudio/dockertest
RUN pwd && \ 
	ls && \
	chmod 755 dockertest.Rproj && \
	ls -la
	
	#Rscript docker_run.R
	# wget https://cran.r-project.org/src/contrib/Archive/spam/spam_2.4-0.tar.gz && \
	# R "print(R.version)" && \
	# R CMD INSTALL /home/rstudio/dockertest/spam_2.4.-0.tar.gz
